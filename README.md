# ﷽

The Full Picture, As-salamu ʿalaykum wa-rahmatullahi wa-barakatuh.

This is a translation from Arabic to English, We are translating Shaykh Ahmad Al-Sayid's "Kamilul Surah"

This is opensourced so that you can suggest edits to the book. :)

You can get started by making an account on gitlab, And clicking on the "TheFullPicture.md" above or clicking this link: https://gitlab.com/al-komi/the-full-picture/-/blob/main/TheFullPicture.md?ref_type=heads and clicking on "Edit", It will tell you that you cant edit files directly and that you need to fork the project, Fork it and make your edits and then it will submit a merge request, And I will review your edit and accept it or reject it or discuss it with you :)

You can find the original arabic for comparison here: https://archive.org/details/20230410_20230410_1932/mode/2up

دمتم في أمان الله | May you always stay in the protection of Allah
